# 101camp1py 课程周刊_v3
~ 预定 190506 2142 发布

-----------------------------------------

先跑起

    Fake it untill make it
    学走之前要先跑
    编程得编程

> 是也乎

-----------------------------------------

- 主编: [101Camp](https://gitlab.com/101camp)
- 责编: [DAMADAMA](https://gitlab.com/101camp/1py/tasks/wikis/dama/HbDamaUsage)


# Progress 
~ 课程活跃情况


课程周任务阶段本周正式过半:

- 意味着距离大家放飞自我, 根据最初愿望开发/规划心中软件之时刻, 只有4周了
- 回想一个月前, 无论之前编程体验是否有
- 现在, 应该已经建立起基本但经典的软件工程概念了:
    + 输入
    + 处理
    + 输出...循环以上
- 而编程自学也和工程一致:
    + 设想
    + 检验
    + 改进...循环以上
- 同时软件本身形成行动也相同:
    + 可运行
    + 追加最小特性
    + 明确...循环以上
- 真正清点一下在蟒营要求以及自己闯过的坑...
- 应该惊讶的发现:
    + 为毛这么简单?
    + 一样的文字, 几天前视为天书
    + 现在忽然就熟视无睹
    + 甚至于自己也开始嗯哼类似的...
- 这就是编程学习真正的秘密
    + 和外语学习,木工学习,钳工学习....
    + 一切技能学习是本质相同的
    + `无它,手熟尔`
    + 上千年前卖油翁早已道出这一秘密
- 只是, 其它教育机构为了本身行业发展
    + 一定要将现成的知识包装成必须有专业指导才可能学会的高级知识
    + 但是, 逻辑上, 一切知识在最开始都是人为自学发现的
    + 从来没有任何一种新知识是有神明来教会第一批用户的
- 所以,蟒营式自学
    + 就是享受这一有组织的系列任务闯关游戏
    + 唯一和纯游戏不同就在
    + 过程中, 一切问题都可以拿出来讨论/交流
    + 而且, 也鼓励分享交流过程中形式的全新的原创体会/经验/代码/...


![190506xkcd](chart/190506xkcd.png)


## 2 weekly activity:
> total commit: **91**


### top5 git commiter
> gen. 190506 205732.792 (by st v.190415.2042)

```
Bleu        : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 26.00
Hu          : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 14.00
liumin101   : ▇▇▇▇▇▇▇▇▇▇▇ 11.00
hstaoqian   : ▇▇▇▇▇▇▇▇▇ 9.00 
charliewu101: ▇▇▇▇▇ 5.00 
```


### all Commit-Comments words
> gen. 2019-05-06 205732.944 (by st v.190415.2042)

```
ZoomQuiet   : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 3694.00
chengyuan101: ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 1695.00
charliewu101: ▇▇▇▇ 454.00
huyuning    : ▇▇▇▇ 428.00
zheng2019   : ▏ 44.00
```



### all actions Issues
> gen. 2019-05-06 205732.369 (by st v.190415.2042)

```
ZoomQuiet   : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 355.00
huyuning    : ▇▇▇▇▇▇▇ 75.00
zheng2019   : ▇▇▇▇▇ 52.00
chengyuan101: ▇▇▇▇ 48.00
tanchangde  : ▇▇▇▇ 41.00
```



### words export for Issues
> gen. 2019-05-06 205732.621 (by st v.190415.2042)

```
ZoomQuiet   : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 89697.00
tanchangde  : ▇▇▇▇▇▇▇▇▇▇▇▇▇ 32654.00
charliewu101: ▇▇▇▇▇▇▇▇ 21864.00
huyuning    : ▇▇▇▇▇▇▇▇ 20802.00
zheng2019   : ▇▇▇▇▇▇▇▇ 20143.00
```


# Stories 
~ 收集各自无法雷同的蠎营真人故事...

## BC
> 血案 ~ BloodCase 自然发生/撞开/生成/... 的工程案件


- [#57](https://gitlab.com/101camp/1py/tasks/issues/57#note_165090074)
- [#68](https://gitlab.com/101camp/1py/tasks/issues/68#note_166834461)
    + @huyuning 
    + ch03 任务理解故事, 对比 ch02 如何加速这一过程?
        * 哪种可以慢速甚至于逐帧播放 gif?
        * 如何写出令小白也理解的任务描述?
    + (金句专业户...)
- [#58](https://gitlab.com/101camp/1py/tasks/issues/58)
    + @hstaoqian101 
    + 到底应该 push 什么文件到仓库中才能令协同合理? 
    + (厘定目标, 期待完成)


## SM
> 隐藏任务激活

- [#65](https://gitlab.com/101camp/1py/tasks/issues/65#note_166834249)
    + @leon_xu 
    + web 界面 gitlab 功能巡演...
    + (厘定边界, 期待完成)
- [#74](https://gitlab.com/101camp/1py/tasks/issues/74#note_166834747)
    + @hstaoqian101 
    + 如何找到高质量的包
    + (厘清目标, 期待嗯哼)




## Scenes
> 经典场景...
### WoW
> 不能不赞叹的...


- [#76](https://gitlab.com/101camp/1py/tasks/issues/76)
    + @chengyuan101 
    + 为什么要以'时间记录'作为作业题目?
    + INSPOINTER.COM 故事
- [#70](https://gitlab.com/101camp/1py/tasks/issues/70#note_166834515)
    + @charliewu101 
    + 科学上网
    + (未能直播分享, 继续期待下周嗯哼)
- [23595b11](https://gitlab.com/101camp/1py/tasks/commit/23595b1138ca40d41f02f5823c1b16319e6a456b#note_166835158)
    + @ljxzsx4 
    + 文章放入kindle?
    + (未能直播分享, 继续期待下周嗯哼)

### SaN
> See as Notsee ~ 视而不见


NIL

## Pythonic
> 蠎味儿 实例行为

### 项目逻辑赋予知识点意义 ～

> Q:ch03 任务理解故事, 对比 ch02 如何加速这一过程?

- ch02 为什么慢?
    + 第一是自己知识少,一无所知时看到任务描述的术语,没办法对任务做任何想象. 
    + 二是没有及时对外求助. 所以加速方法一是多存知识,二是及时向外求助. 
- 上周日开完周会后,看到有作业提交上来,
    + 我研究了完成同学的代码,自己也做出来后,
    + 算是彻底理解了任务. 
- 这周 ch03 在我已经理解 ch02 的基础上,
    + 看到只有 * 功能是新的,
    + 再仔细读和看演示,就知道是对历史所有数据做一个处理和分析,
    + 所以这周任务理解问题不大. 


> Q:如何写出令小白也理解的任务描述?

- 任务理解太抽象和描述化是因为专家按照自己的脑回路去写的. 
- 他们不知道小白会卡在哪里,如果知道小白卡在哪里,可破的概率大一些. 
- 其实我这周亲自体验了两个形态,
    + 一点都不懂任务在说什么到自己写出总结时也变得任务描述化,
    + 我也已经完全理解了为什么用 ff,gg这些抽象测试符号了. 

>(咆哮:在改了 n 次代码后,输入了 n 次后,怎么可能还用 study sleep 这些费力但有意义的符号呢?)

- 再次深刻认识到:
    + 小白状态时的问题,真的是有了相关知识后无法想象的!
    + 大妈写任务时,怎么能够想到有人会认为 ff ,gg 是输出项呢?
    + 但是这就是小白时卡了好多天的问题呐!
- 综上,写小版本的任务理解需要 :
    + 1.知道小白卡哪个地方,哪个术语?
    + 2.按照他们的脑回路写出的就是通俗的了 
- 所以`小白状态`时的问题和脑回路:
    + **特!别!宝!贵!**


> Q: 如何知道小白的问题,并写出通俗的任务理解?

- 及时互动,一同改写. 
- 可以规定:
    + 周几几点,卡在理解任务无从下手,
        * 那么就让学员发 issue 提问. 
        * 如此,专家知道后,在 issue 中与其互动,而后有针对性拆解,
    + 一同改写,沉淀下来以后也可以复用. 
- 运用历史资料. 
    + 往期学员有没有沉淀下来的资料,笔记呢?
    + 这些资料会有些许线索,若能得到这些问题,可提前预防. 
    + (鼓励提问是一个艰辛的过程 ...... 而且小白状态时很羞于提问,会觉得自己的问题过于简单,不值得研究. )
- 逆向工程,我发现写程序是抽象化的过程,
    + 若真的不知道小白卡在哪里,
    + 书写时赋予抽象意义化,具体具体再具体,
    + 举例子等方式都可尝试,这样大家更能感知到要表达什么. 



# Recommedations  
~ 嗯哼各种场景中发现的嗯哼...

- 是也乎:
    + [ch03 知识图谱](http://io.101.camp/)
    + [Issue 366 . 蠎周刊 .汇集全球蠎事儿 ;-)](http://weekly.pychina.org/issue/issue-366.html)


# Postscript 后记 
~ 蠎营周刊是什么以及为什么和能怎么...

大妈曰过: `参差多态 才是生机`

问题在 `参差` 的行为是无法形成团队的

    Coming together is a beginning; 
    Keeping together is progress; 
    Working together is success!

<--- [Henry Ford](https://www.brainyquote.com/quotes/quotes/h/henryford121997.html)

- 所以, 有了 大妈 随见随怼的持续嗯哼...
- 但是, 想象一年后, 回想几十周前自己作的那些 `图样图森破` 
    + 却没现成的资料来出示给后进来嗯哼?
    + 不科学, 值得记录的, 就应当有个形式固定下来
- 所以,有了这个 `蠎营周刊` (101Weekly)

:

    What is Pythonic?
    Why we make Pythonic?
    What are the possibilities of Pythonic?


That's why Dama keeps on debugging.
However, as time goes by, maybe you would not remember these days clearly and spread your experience difficultly.
What a pity!
The valuable should have a fixed form to be recorded.
That's why we make the Weekly for 101.camp.

