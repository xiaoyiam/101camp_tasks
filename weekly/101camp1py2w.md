# 101camp1py 课程周刊_v2
~ 预定 190429 2142 发布

-----------------------------------------

Unknow Unknow

    不知小白何不知
    但期自吼哪不明
    碧空五四天

> DAMADAMA

-----------------------------------------

- 主编: [101Camp](https://gitlab.com/101camp)
- 责编: [DAMADAMA](https://gitlab.com/101camp/1py/tasks/wikis/dama/HbDamaUsage)


# Progress 
~ 课程活跃情况

进入 ch3 任务周, 同时又是 5.1+54 小长徦:

- 以往的课程经验, 学员在这种时期的时间投入是两极分化的
- 单身狗, 一般可以自由选择猫在家极大时间投入课程
- 有粮有家的, 一般不得不陪各种领导吃吃吃...可能投入时间极大减少
- 但是, 可以肯定的:
    + 蟒营, 任务已经为大家认可
    + 甚至于, 一些学员已经受不了这么粗糙的功能要求
    + 开始主动加戏 ~ 哈, 不是, 是主动追加要求
    + 和 Alfred 结合, 和 gitlab 结合...
    + 这真是可怕的主观能动性
    + 回想当年在学校时,都是比谁听话
    + 结果是在工作四年后, 才突然反应过来, 谁听话谁吃亏
        * 只有超额完成任务
        * 才可能获得晋升
        * 而, 超额就得独立发现任务背后真正的问题, 并独力解决
- 所以, 只能期待在编程之路上开始飚车的学员
    + 嫑忘记分享
    + 嫑忘记分享
    + 嫑忘记分享
    + ... 这样无论学员还是蟒营本身, 才能更好的学习如何学习哪


![190429xkcd](chart/190429xkcd.png)

## 2 weekly activity:
> total commit: **111**


### top5 git commiter
> gen. 190429 204316.774 (by st v.190415.2042)

```
hstaoqian   : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 15.00
Changde     : ▇▇▇▇▇▇▇▇▇▇▇▇▇ 13.00
liumin101   : ▇▇▇▇▇▇▇▇▇▇▇▇ 12.00
charliewu101: ▇▇▇▇▇▇▇▇▇ 9.00 
nana        : ▇▇▇▇▇▇▇▇▇ 9.00 
```


### all Commit-Comments times
> gen. 2019-04-29 204316.845 (by st v.190415.2042)

```
ZoomQuiet : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 24.00
doituself : ▇▇▇▇▇▇▇▇▇▇▇▇ 12.00
jami101   : ▇▇▇▇▇▇▇ 7.00 
tanchangde: ▇▇▇▇▇▇ 6.00 
huyuning  : ▇▇▇▇▇ 5.00 
```


### all Commit-Comments words
> gen. 2019-04-29 204316.921 (by st v.190415.2042)

```
doituself : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 1102.00
ZoomQuiet : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 1016.00
jami101   : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 500.00
tanchangde: ▇▇▇▇▇▇▇▇ 268.00
huyuning  : ▇▇▇ 111.00
```



### Issues notes actions
> gen. 2019-04-29 204316.520 (by st v.190415.2042)

```
ZoomQuiet   : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 301.00
huyuning    : ▇▇▇▇▇ 47.00
zheng2019   : ▇▇▇▇▇ 42.00
tanchangde  : ▇▇▇▇ 39.00
chengyuan101: ▇▇▇▇ 37.00
```


### words export for Issues
> gen. 2019-04-29 204316.612 (by st v.190415.2042)

```
ZoomQuiet   : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 86649.00
tanchangde  : ▇▇▇▇▇▇▇▇▇▇▇▇▇ 32654.00
charliewu101: ▇▇▇▇▇▇▇▇ 21467.00
zheng2019   : ▇▇▇▇▇▇▇▇ 20050.00
chengyuan101: ▇▇▇▇▇▇▇ 17293.00
```


# Stories 
~ 收集各自无法雷同的蠎营真人故事...


## BC
> 血案 ~ BloodCase 自然发生/撞开/生成/... 的工程案件

- [#58](https://gitlab.com/101camp/1py/tasks/issues/58#note_164892786)
    + @hstaoqian101 
    + 什么值得 push / git 探险经验
    + => SM: git 合意文件判定标准和意外处置手册

## SM
> 隐藏任务激活

- [#67](https://gitlab.com/101camp/1py/tasks/issues/67#note_164821531)
    + @linfeng101
    + gif 制作教程
- [#65](https://gitlab.com/101camp/1py/tasks/issues/65#note_164821763)
    + @leon_xu 
    + web 界面 gitlab 核心功能操作指南
- [#74](https://gitlab.com/101camp/1py/tasks/issues/74)
    + @hstaoqian101 
    + 如何找到高质量的包

## Scenes
> 经典场景...


### WoW
> 不能不赞叹的...

- [#52](https://gitlab.com/101camp/1py/tasks/issues/52#note_164822194)
    + @charliewu101 
    + 周任务总结以及总结本身的具体操作?

### SaN
> See as Notsee ~ 视而不见


NIL

## Pythonic
> 蠎味儿 实例行为

- [#68](https://gitlab.com/101camp/1py/tasks/issues/68#note_164821306)
    + @huyuning 
    + 一起界定 术语 困境?

意外的进入了一种快速迭代文案的交流:

### 原案:

- 总目标不变:
    + 记录每次投入 `101camp1py` 课程真正用时
- 向实用化进步一点点儿:
    + 能每次进行注释, 说明自学行为分类/内容
    + 能自动记录以往所有记录的时间段,并回放
    + 而且能指定历史记录, 修改注释

### 大妈:

- 总目标不变:
    + 记录每次投入 `101camp1py` 课程真正用时
- 向实用化进步一点点儿:
    + 每次计时时可以追加 说明:
        * 用来注释本次自学行为的分类/内容
        * 可以在计时开始或是结束时附加说明
    + 能自动记录以往所有记录的时间段,并回放
        * 即便关闭了程序, 每次运行时
        * 只要用户想, 都可以用一个指令查阅历史记录数据(即时间段)
    + 而且能指定历史记录, 修改注释
        * 可以通过某种形式/指令
        * 修改历史上某次时间帐单记录的附加说明


### 小白:
>  @huyuning 的思路为增加这项功能的`应用场景`和`给出具体事例`,`把测试字符具体化意义化`,从而是在说小白能听懂的人话呐!!!尝试如下

* 总目标不变:
  * 记录每次投入 `101camp1py` 课程真正用时
* 向实用化进步一点点儿:
  * 每次计时时可以追加 说明:
    * 用来注释本次自学行为的分类/内容 (比如,备注说明这段时间是`在学python` 还是在 `听歌`)
    * 可以在计时开始或是结束时附加说明
  * 能自动记录以往所有记录的时间段,并回放
    * 即便关闭了程序, 每次运行时
    * 只要用户想, 都可以用一个指令查阅历史记录数据(即时间段)
  * 而且能指定历史记录, 修改注释
    * 可以通过某种形式/指令
    * 修改历史上某次时间帐单记录的附加说明 (比如,刚刚记录了一个时间段是`在学python`,但是后来,这段时间其实听歌了,这时可以修改此条记录为`听歌`. )


### 是也乎
> 无法同意更多...


高兴 @huyuning  又冒出一则金句哪:

> 不知道小白不知道什么

的确,不过, 蟒营和其它课程, 最大的不同, 应该就在这儿了:

- 我们知道我们不知道小白不知道什么
- 所以, 自始至终都在尝试引导小白主动表达自己不知道什么
- 进而, 得以针对性的帮助
- 其它课程不关心这个艰难的再学习过程...

进一步的, 提醒:

- `...等我理解好了去改写`
- 千万别等...这就是 MVP 的开始哪:
    + 您先根据您现在理解的给出一个版本
    + 俺们一看发现有重大偏差, 那么有可对比版本
    + 也就知道哪行文字开始出了问题
    + 就可以给出大妈版的又一个尝试版本
    + 您再来瞧, 还是有不明白的
    + 于是, 再来一个你的版本
    + ...
- 这样高速一来一去, 就比你单方面死磕来的高效了
- 同时, 这一相互改进的过程又公开在 Issue 中
    + 其它学员也可以一起参与
    + 这才是蟒营课程最期待的状态:
        * 课程是学员的
        * 蟒营只是创造了一个大家一起学习的场景
        * 如何/何时/怎样/...提供服务
            - 都是蟒营在努力自学的命题
            - 请大家给大妈们创造服务大家的机会


# Recommedations  
~ 嗯哼各种场景中发现的嗯哼...

- 是也乎:
    + [Issue 365 ~ 蠎周刊 ~ 汇集全球蠎事儿 ;-)](http://weekly.pychina.org/issue/issue-365.html)


# Postscript 后记 
~ 蠎营周刊是什么以及为什么和能怎么...

大妈曰过: `参差多态 才是生机`

问题在 `参差` 的行为是无法形成团队的

    Coming together is a beginning; 
    Keeping together is progress; 
    Working together is success!

<--- [Henry Ford](https://www.brainyquote.com/quotes/quotes/h/henryford121997.html)

- 所以, 有了 大妈 随见随怼的持续嗯哼...
- 但是, 想象一年后, 回想几十周前自己作的那些 `图样图森破` 
    + 却没现成的资料来出示给后进来嗯哼?
    + 不科学, 值得记录的, 就应当有个形式固定下来
- 所以,有了这个 `蠎营周刊` (101Weekly)

:

    What is Pythonic?
    Why we make Pythonic?
    What are the possibilities of Pythonic?


That's why Dama keeps on debugging.
However, as time goes by, maybe you would not remember these days clearly and spread your experience difficultly.
What a pity!
The valuable should have a fixed form to be recorded.
That's why we make the Weekly for 101.camp.

