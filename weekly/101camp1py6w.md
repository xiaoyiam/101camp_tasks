# 101camp1py 课程周刊_v6
~ 预定 190527 2142 发布

-----------------------------------------

比较快

    以往尝试很多次
    放弃总在一瞬间
    雷暴没商量

> DAMADAMA

-----------------------------------------

- 主编: [101Camp](https://gitlab.com/101camp)
- 责编: [DAMADAMA](https://gitlab.com/101camp/1py/tasks/wikis/dama/HbDamaUsage)


# Progress 
~ 课程活跃情况

嗯哼?

- 大家针对代码的讨论上周只有一次...
- 这说明两个可能:
    + 大家都已有足够代码感觉, 更多关注功能架构/布局和体验, 具体代码技巧已经不太关注
    + 或相反, 部分同学至今没突破基础代码理解能力, 窜门时,太多看不明白, 以致无法提问了
- 这两个可能其实, 都不是好事儿:
    + 功能的整体架构/布局/...一样得落实到代码, IDD 也是每个构想/猜想都应该对应到代码
    + 代码也是文本, 只是比较特殊, 和任何语言/文字一样, 只有多看多用才可能熟练
- 说穿了...和 @huyuning 分享的经验一样:
    + 只有先说服自己不怕, 一定学的会/看得懂
    + 然后,自然就写的出, 调试的通...
    + 否则, 永远在潜意识中给自己制造压力
    + 那么, 焦虑和 bug 一样是会自动增生的...
- 好在, 无论何时开始, 都不算迟:
    + 蟒营课程设计特点就是:
        * 再次强调
        * 任何一周任务硬头皮去完成
        * 就等于完成对应过往所有周任务
    + 所以, 最后一周具体任务, 其实只是对以往所有开发任务的一次整理
    + 用自动化将所有环节程序化关注起来
    + 这才算形成一个完整的工程实体
- 加油, 为那个想编程的自己.

![190527xkcd](chart/190527xkcd.png)

## 2 weekly activity:
> total commit: **18**


### top5 git commiter
> gen. 190527 180330.156 (by st v.190415.2042)

```
Hu       : ▇▇▇▇▇▇ 6.00 
liumin101: ▇▇▇ 3.00 
hstaoqian: ▇▇ 2.00 
Bleu     : ▇▇ 2.00 
unclejia : ▇▇ 2.00 

```


### all Commit-Comments times
> gen. 2019-05-27 180330.224 (by st v.190415.2042)

```
ljxzsx4: ▇ 1.00 

```

### all Commit-Comments words
> gen. 2019-05-27 180330.281 (by st v.190415.2042)

```
ljxzsx4: ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 23.00

```

### all actions Issues
> gen. 2019-05-27 180329.904 (by st v.190415.2042)

```
ZoomQuiet   : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 475.00
huyuning    : ▇▇▇▇▇▇▇ 96.00
chengyuan101: ▇▇▇▇ 63.00
ljxzsx4     : ▇▇▇▇ 62.00
zheng2019   : ▇▇▇ 52.00
```



### words export for Issues
> gen. 2019-05-27 180330.031 (by st v.190415.2042)


```
ZoomQuiet   : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 104192.00
tanchangde  : ▇▇▇▇▇▇▇▇▇▇▇ 32854.00
huyuning    : ▇▇▇▇▇▇▇▇▇ 27922.00
hstaoqian101: ▇▇▇▇▇▇▇▇ 23476.00
charliewu101: ▇▇▇▇▇▇▇ 21765.00
```

# Stories 
~ 收集各自无法雷同的蠎营真人故事...

## BC
> 血案 ~ BloodCase 自然发生/撞开/生成/... 的工程案件


- [#97](https://gitlab.com/101camp/1py/tasks/issues/97#note_171776978)
    + @huyuning 
    + Heroku 体验
    + 是也乎:
        * 嘦敢看文档
        * 一切就变成水到渠成...

## SM
> 隐藏任务激活

- [#74](https://gitlab.com/101camp/1py/tasks/issues/74#note_166834747)
    + @hstaoqian101 
    + 如何找到高质量的包
    + 是也乎:
        * 从源头
        * 看社区
        * 测质量
        * 定需求



## Scenes
> 经典场景...

### WoW
> 不能不赞叹的...


- [#104](https://gitlab.com/101camp/1py/tasks/issues/104#note_173340007)
    + @huyuning 
    + slash command 极速完成
- [#70](https://gitlab.com/101camp/1py/tasks/issues/70#note_171776647)
    + @charliewu101 
    + 科学上网+++
- [#76](https://gitlab.com/101camp/1py/tasks/issues/76#note_168910180)
    + @chengyuan101 
    + `激发点` 项目设计连载


### SaN
> See as Notsee ~ 视而不见


NIL

## Pythonic
> 蠎味儿 实例行为

@huyuning 

- 勇于心理建设
- 猛于时间投入
- 那么一切将不同...

> 上周胃疼 + web任务的正反馈比较少，
> 编程全断了五六天，一度想要放弃。
> 今天内耗结束，打开了gitlab，跟着 heroku 文档试探起这周编程任务。做还是不做？【只在一念之间】，还是不能放弃呀~🌶

是也乎,(￣▽￣)

    放弃只是一闪念
    反正总是有理由
    多数当然不得己
    回想当年总这样
    至今一事无成立

    点解点解
        立即开始
            莫想莫想
                失败乍办

# Recommedations  
~ 嗯哼各种场景中发现的嗯哼...

- 是也乎:
    + [ch06 知识图谱](http://io.101.camp/)
    + [Issue 369 ~蠎周刊 ~汇集全球蠎事儿 ;-)](http://weekly.pychina.org/issue/issue-369.html)


# Postscript 后记 
~ 蠎营周刊是什么以及为什么和能怎么...

大妈曰过: `参差多态 才是生机`

问题在 `参差` 的行为是无法形成团队的

    Coming together is a beginning; 
    Keeping together is progress; 
    Working together is success!

<--- [Henry Ford](https://www.brainyquote.com/quotes/quotes/h/henryford121997.html)

- 所以, 有了 大妈 随见随怼的持续嗯哼...
- 但是, 想象一年后, 回想几十周前自己作的那些 `图样图森破` 
    + 却没现成的资料来出示给后进来嗯哼?
    + 不科学, 值得记录的, 就应当有个形式固定下来
- 所以,有了这个 `蠎营周刊` (101Weekly)

:

    What is Pythonic?
    Why we make Pythonic?
    What are the possibilities of Pythonic?


That's why Dama keeps on debugging.
However, as time goes by, maybe you would not remember these days clearly and spread your experience difficultly.
What a pity!
The valuable should have a fixed form to be recorded.
That's why we make the Weekly for 101.camp.

