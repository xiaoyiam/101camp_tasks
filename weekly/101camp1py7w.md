# 101camp1py 课程周刊_v7
~ 预定 190603 2142 发布

-----------------------------------------

黄梅天

    协同向来不简单
    团队不是团伙凑
    能技灿烂时

> DAMADAMA

-----------------------------------------

- 主编: [101Camp](https://gitlab.com/101camp)
- 责编: [DAMADAMA](https://gitlab.com/101camp/1py/tasks/wikis/dama/HbDamaUsage)


# Progress 
~ 课程活跃情况

终于 `时帐` 系列坑任务告一段落了...

- 大家被迫高速体验了一下软件这150年的变化
    + CLI->local web->world web->PaaS->CI/CD
    + 中间还涉及了各种常用/通用/著名内建/第三方模块
- 当然, 最关键的是知道什么是编程了吧...
- 和以往自己猜想的完全不同...
- 简单 low 到难以启齿:
    + 这也是为什么, 其它网络课程都死命展示编程困难的那些点
    + 而将,编程真正日常面对的问题, 谁都能解决的问题隐藏起来
    + 以免, 大家对软件工程师这一行业又产生另外一个极端印象
- 但是, 蟒营 不怕揭示公开编程的秘密:
    + 编程技能困难的, 并不在编程技术本身
    + 而在编程活动和以往不同的交流思路/模式/工具/习语
    + 以及高度自律的独立思考问题习惯
- 所以, 真正难点在这最后3周:
    + 如果最快吸引到足够的注意力
    + 并逼自己担当团队 leader 快速分配任务
    + 并完成按时完成
- 当然, 俺还是得提醒技巧的:
    + 和中学演讲技巧一样
    + 想感动别人, 先感动自己
    + 发挥想象力, 将作品完成后带来的一切美好先描述出来
    + 自然, 无论自己还是小伙伴们, 就都有了闯关的冲动和渴望


![190603xkcd](chart/190603xkcd.png)

等等, 为什么 6.1 这么多 Issue 的行为?

## 2 weekly activity:
> total commit: **25**


### top5 git commiter
> gen. 190603 210139.771 (by st v.190415.2042)

```
Hu       : ▇▇▇▇▇▇ 6.00 
unclejia : ▇▇▇▇▇▇ 6.00 
linfeng  : ▇▇▇▇▇ 5.00 
Changde  : ▇▇▇▇▇ 5.00 
hstaoqian: ▇▇ 2.00 
```


### all Commit-Comments times
> gen. 2019-06-03 210139.838 (by st v.190415.2042)

```
linfeng101: ▇ 1.00 
```


### all Commit-Comments words
> gen. 2019-06-03 210139.896 (by st v.190415.2042)

```
linfeng101: ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 22.00
```


### all actions Issues
> gen. 2019-06-03 210139.524 (by st v.190415.2042)

```
ZoomQuiet   : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 515.00
huyuning    : ▇▇▇▇▇▇▇ 104.00
ljxzsx4     : ▇▇▇▇ 67.00
chengyuan101: ▇▇▇▇ 63.00
hstaoqian101: ▇▇▇▇ 60.00
```



### words export for Issues
> gen. 2019-06-03 210139.646 (by st v.190415.2042)

```
ZoomQuiet   : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 114676.00
tanchangde  : ▇▇▇▇▇▇▇▇▇▇ 32854.00
huyuning    : ▇▇▇▇▇▇▇▇▇ 28704.00
hstaoqian101: ▇▇▇▇▇▇▇▇ 26711.00
charliewu101: ▇▇▇▇▇▇ 21765.00
```



# Stories 
~ 收集各自无法雷同的蠎营真人故事...

## BC
> 血案 ~ BloodCase 自然发生/撞开/生成/... 的工程案件

- [#111](https://gitlab.com/101camp/1py/tasks/issues/111#note_175431628)
    + @huyuning 
    + 论`比如`的理解

## SM
> 隐藏任务激活

- 毕业 Tee 快递地址问卷
- What is 101.camp?
- 提问的智慧?


## Scenes
> 经典场景...

### WoW
> 不能不赞叹的...

- [#107](https://gitlab.com/101camp/1py/tasks/issues/107#note_176865822)
    + @cecila 
    + 归来的体验
    + 是也乎:
        * 没 got 到邀请, 但是, 从行为看, 学员们积极积累下来的嗯哼
        * 令后进学员`抄`的非常爽快
        * 其实编程中的抄, 不象写作中的抄, 最终是否抄对味儿了有标准裁判的
            - 编译器说对,
            - 就是对
        * 所以, 编程行为中的抄, 就是核心技术
- [#76](https://gitlab.com/101camp/1py/tasks/issues/76#note_168910180)
    + @chengyuan101 
    + `激发点` 项目设计连载
    + 是也乎:
        * 其实, 当初自爆出这一从严概念时, 就知道
        * 这次课程, 毕业大作品, 这个 `激发点` 就是个好点
        * 只差创始人, 认真将之嗯哼出个原型了.
        * 当然, 和其它创业公司类似
            - 就差一个程序猿...


## Pythonic
> 蠎味儿 实例行为

NIL


# Recommedations  
~ 嗯哼各种场景中发现的嗯哼...

- 是也乎:
    + [ch07 知识图谱](http://io.101.camp/)
    + [Issue 370 ~蠎周刊 ~汇集全球蠎事儿 ;-)](http://weekly.pychina.org/issue/issue-370.html)


# Postscript 后记 
~ 蠎营周刊是什么以及为什么和能怎么...

大妈曰过: `参差多态 才是生机`

问题在 `参差` 的行为是无法形成团队的

    Coming together is a beginning; 
    Keeping together is progress; 
    Working together is success!

<--- [Henry Ford](https://www.brainyquote.com/quotes/quotes/h/henryford121997.html)

- 所以, 有了 大妈 随见随怼的持续嗯哼...
- 但是, 想象一年后, 回想几十周前自己作的那些 `图样图森破` 
    + 却没现成的资料来出示给后进来嗯哼?
    + 不科学, 值得记录的, 就应当有个形式固定下来
- 所以,有了这个 `蠎营周刊` (101Weekly)

:

    What is Pythonic?
    Why we make Pythonic?
    What are the possibilities of Pythonic?


That's why Dama keeps on debugging.
However, as time goes by, maybe you would not remember these days clearly and spread your experience difficultly.
What a pity!
The valuable should have a fixed form to be recorded.
That's why we make the Weekly for 101.camp.

