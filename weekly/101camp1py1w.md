# 101camp1py 课程周刊_v1
~ 预定 190422 2142 发布

-----------------------------------------

嗯哼

    四月芳菲始烧尽
    周坑已将第三波
    平常心就好

> DAMADAMA

Humm

    spring end with April
    weekly tasks welcome the third
    just general

>> DAMADAMA

-----------------------------------------

- 主编: [101Camp](https://gitlab.com/101camp)
- 责编: [DAMADAMA](https://gitlab.com/101camp/1py/tasks/wikis/dama/HbDamaUsage)


# Progress 
~ 课程活跃情况

进入 ch2 第三周了
(是的, ch0 也有一周呢, Python 列表也都是从 0 开始的哪.)

- 同学们在持续惊吓着课程方:
    + 有的同学不由自主的用 uve.js 完成了交互式 SPA
    + 有的同学难以控制的将 myTL 和 Alfred 结合了起来
    + 还有的...
- 当然,还有部分同学,因为各种原因还没进入蟒营节奏
    + 有的还未将课程仓库 clone 到本地
    + 有的还不知道在 Slack 哪个频道和大家交流
    + 有的甚至一直就没看到开课通告邮件
    + 还有的...
- 但是, 无论已经 high 起来的, 还是依然困惑的
    + 只要开始就对了一切
    + 毕竟, 蟒营 课程是陪伴式私塾辅导
    + 完全根据大家的嗯哼来嗯哼
- 所以, 开始嗯哼吧
    + 针对每周任务
    + 以及, 周任务是迭代式的
    + 也就是说, 如果中间有任务没作
    + 那么, 新的周任务来了
    + 跳过中间的, 直接来完成当周的
    + 也没有任何问题:
        * 因为, 每一周的任务
        * 本质上都包含了以往所有周任务包含的所有知识点
        * 只要刷下来
        * 那么, 以往所有周的知识点都没错过 ;-)


![190422xkcd](chart/190422xkcd.png)

## 2 weekly activity:
> total commit: **153**


### top5 git commiter
> gen. 190422 200139.170 (by st v.190415.2042)

```
Changde   : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 33.00
zhouyinyun: ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 17.00
Zoom.Quiet: ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 15.00
linfeng   : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 14.00
hstaoqian : ▇▇▇▇▇▇▇▇▇▇▇▇ 12.00
```


### all Commit-Comments times
> gen. 2019-04-22 200139.238 (by st v.190415.2042)

```
ZoomQuiet : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 24.00
doituself : ▇▇▇▇▇▇▇▇▇▇▇▇ 12.00
jami101   : ▇▇▇▇▇▇▇ 7.00 
tanchangde: ▇▇▇▇▇▇ 6.00 
huyuning  : ▇▇▇▇▇ 5.00 
```


### all Commit-Comments words
> gen. 2019-04-22 200139.317 (by st v.190415.2042)

```
doituself : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 1102.00
ZoomQuiet : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 1016.00
jami101   : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 500.00
tanchangde: ▇▇▇▇▇▇▇▇ 268.00
huyuning  : ▇▇▇ 111.00
```



### all actions Issues
> gen. 2019-04-22 200138.863 (by st v.190415.2042)

```
ZoomQuiet   : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 218.00
tanchangde  : ▇▇▇▇▇▇▇ 43.00
chengyuan101: ▇▇▇▇▇▇ 37.00
near8023    : ▇▇▇▇ 30.00
huyuning    : ▇▇▇▇ 30.00
```



### words export for Issues
> gen. 2019-04-22 200139.024 (by st v.190415.2042)

```
ZoomQuiet   : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 64868.00
tanchangde  : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 34240.00
zheng2019   : ▇▇▇▇▇▇▇▇▇▇ 18584.00
chengyuan101: ▇▇▇▇▇▇▇▇ 15203.00
zhouyinyun  : ▇▇▇▇▇▇▇ 13229.00
```

嗯哼, 可见大妈是话唠无疑了...


# Stories 
~ 收集各自无法雷同的蠎营真人故事...


Q: 为啥两个一样的文件夹能共存 ?

是也乎,(￣▽￣)

- 可能性有很多...
- 不过, 每一个都不等于你现在的设想 '

Q: 我在终端报错的时候 操作了两次 是因为这样吗 ?

是也乎,(￣▽￣)

    0: 在进行什么操作?
    1: 报了什么错?
    2: 两次一样?
    3: 以上如何检验操作以及结果?

- 所以, 嘦涉及技术问题,
- 总是能合理分解为更小更具体的过程/数据/操作/信息...
- 只有用 Issue 合理清晰的记述下来
- 其它人才可能复现相同现象
- 才可能进一步分享对应经验和建议...
- 所以, 来吧, 用 Issue 提问 




## BC
> 血案 ~ BloodCase 自然发生/撞开/生成/... 的工程案件

- [#58](https://gitlab.com/101camp/1py/tasks/issues/58)
    + @hstaoqian101 到底应该 push 什么文件到仓库中?


## SM
> 隐藏任务激活

- [#46](https://gitlab.com/101camp/1py/tasks/issues/46#note_161551748)
    + @soloman 蟒营用git最小手册
- [#55](https://gitlab.com/101camp/1py/tasks/issues/55#note_162687143)
    + @nocynic windows 不慌张


## Scenes
> 经典场景...

### WoW
> 不能不赞叹的...

- [3b2ab51f](https://gitlab.com/101camp/1py/tasks/commit/3b2ab51fe6b83502e8487e4c0a33578c37694d78#note_162593263)
    + @jami101 为何如此顺畅?



### SaN
> See as Notsee ~ 视而不见

- [#51](https://gitlab.com/101camp/1py/tasks/issues/51#note_162594123)
    + @zheng2019 en 恐惧症?


## Pythonic
> 蠎味儿 实例行为

Alfred+Python

>> 这个可以有 ;-)




# Recommedations  
~ 嗯哼各种场景中发现的嗯哼...

- 是也乎:
    + [Issue 364 |蠎周刊 |汇集全球蠎事儿 ;-)](http://weekly.pychina.org/issue/issue-364.html)

## PS: What is 101.camp?
>  什么是蠎营?


- 时限: 8周
- 范畴: 所有学员
- 任务: 尝试用自己的体验和理解来描述什么是蠎营?
    + 能说服半年前的自己参加
    + 或是, 想象向未来新学员来介绍
    + 指标: 512字以上
        * 可以多人合作撰写


## PPS: How-To-Ask-Questions-The-Smart-Way
> 提问的智慧


- 时限: 9周
- 范畴: 所有学员
- 任务: 课程期间
    + 至少亲自通读一次 提问的智慧
    + 回复对应例蟒 Issue 说明最触动自己的一点
- 指标: 420 字以上


# Postscript 后记 
~ 蠎营周刊是什么以及为什么和能怎么...

大妈曰过: `参差多态 才是生机`

问题在 `参差` 的行为是无法形成团队的

    Coming together is a beginning; 
    Keeping together is progress; 
    Working together is success!

<--- [Henry Ford](https://www.brainyquote.com/quotes/quotes/h/henryford121997.html)

- 所以, 有了 大妈 随见随怼的持续嗯哼...
- 但是, 想象一年后, 回想几十周前自己作的那些 `图样图森破` 
    + 却没现成的资料来出示给后进来嗯哼?
    + 不科学, 值得记录的, 就应当有个形式固定下来
- 所以,有了这个 `蠎营周刊` (101Weekly)

:

    What is Pythonic?
    Why we make Pythonic?
    What are the possibilities of Pythonic?


That's why Dama keeps on debugging.
However, as time goes by, maybe you would not remember these days clearly and spread your experience difficultly.
What a pity!
The valuable should have a fixed form to be recorded.
That's why we make the Weekly for 101.camp.

