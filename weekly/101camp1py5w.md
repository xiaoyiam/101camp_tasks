# 101camp1py 课程周刊_v5
~ 预定 190520 2142 发布

-----------------------------------------

慢慢来

    世间琐事永难净
    何不多合知行一
    苦夏自甘心


> 是也乎

-----------------------------------------

- 主编: [101Camp](https://gitlab.com/101camp)
- 责编: [DAMADAMA](https://gitlab.com/101camp/1py/tasks/wikis/dama/HbDamaUsage)


# Progress 
~ 课程活跃情况


进入 ch06 其实总第7周了:

- 从活跃数来看多数学员上周没能及时进入 ch05 任务
- 通过回访学员也证实了这点目测:
    + 主要原因也没意外
        * 太忙
        * 太累
        * 太难
        * ...
- 但是, 依然有学员决然放弃完美任务交付
- 立即, 进入 ch06 任务, 来体验真实世界项目样缺憾
- 这, 其实才是正确姿势:
    + 所谓, 留白天地宽
    + 课程长达两个半月
        * 期间发生任何事情都有可能
        * 完美达成所有周任务,并及时交付, 几乎不可能
        * 那就别强求 ;-)
    + 放下不可避免的遗憾
        * 立即开始新的挑战
        * 才能守住持续成就感
        * 并加强新自学习惯
        * 以周迭代节奏, 高速夯实全新思维结构
- 预告:
    + 下周将进入毕业大作业
    + 也将开展问卷调查
    + 请学员们时刻作好准备.


![190520xkcd](chart/190520xkcd.png)

## 2 weekly activity:
> total commit: **24**


### top5 git commiter
> gen. 190520 201802.822 (by st v.190415.2042)

```
hstaoqian  : ▇▇▇▇▇▇ 6.00 
nana       : ▇▇▇▇▇ 5.00 
hechengyuan: ▇▇▇ 3.00 
liumin101  : ▇▇▇ 3.00 
Bleu       : ▇▇ 2.00 
```



### all Commit-Comments times
> gen. 2019-05-20 201802.906 (by st v.190415.2042)

```
chengyuan101: ▇▇▇▇▇▇▇▇ 8.00 
ZoomQuiet   : ▇▇▇▇▇▇ 6.00 
near8023    : ▇▇▇▇ 4.00 
hstaoqian101: ▇ 1.00 
```


### all Commit-Comments words
> gen. 2019-05-20 201802.997 (by st v.190415.2042)

```
chengyuan101: ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 721.00
ZoomQuiet   : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 538.00
near8023    : ▇▇▇▇ 88.00
hstaoqian101: ▇ 22.00
```



### all actions Issues
> gen. 2019-05-20 201802.401 (by st v.190415.2042)

```
ZoomQuiet   : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 456.00
huyuning    : ▇▇▇▇▇▇ 83.00
chengyuan101: ▇▇▇▇ 63.00
ljxzsx4     : ▇▇▇▇ 52.00
zheng2019   : ▇▇▇▇ 52.00
```



### words export for Issues
> gen. 2019-05-20 201802.629 (by st v.190415.2042)


```
ZoomQuiet   : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 102054.00
tanchangde  : ▇▇▇▇▇▇▇▇▇▇▇ 32784.00
hstaoqian101: ▇▇▇▇▇▇▇▇▇ 26084.00
huyuning    : ▇▇▇▇▇▇▇ 22402.00
charliewu101: ▇▇▇▇▇▇▇ 21765.00
```



# Stories 
~ 收集各自无法雷同的蠎营真人故事...

## BC
> 血案 ~ BloodCase 自然发生/撞开/生成/... 的工程案件

- [#95](https://gitlab.com/101camp/1py/tasks/issues/95#note_171776864)
    + (请假推迟) @huyuning 
    + 解析任务思考/分解 过程



## SM
> 隐藏任务激活

- [#70](https://gitlab.com/101camp/1py/tasks/issues/70#note_171776647)
    + (请假推迟) @charliewu101 
    + 科学上网+++
- [#74](https://gitlab.com/101camp/1py/tasks/issues/74#note_166834747)
    + (请假推迟) @hstaoqian101 
    + 如何找到高质量的包


## Scenes
> 经典场景...

### WoW
> 不能不赞叹的...

- [#98](https://gitlab.com/101camp/1py/tasks/issues/98#note_171777079)
    + @hstaoqian101 
    + 构思远程使用 Alfred 
- [#97](https://gitlab.com/101camp/1py/tasks/issues/97#note_171776978)
    + (请假推迟) @huyuning 
    + Heroku 体验




### SaN
> See as Notsee ~ 视而不见


> 什么是蟒营,这个问题我参加原型班时被某大妈问过～

我现在的理解如下,供你参考:

- 蟒营引导学员直奔章节任务,并给予必要提示. 让学员依据自身知识背景,技能层次碰撞激发个性化问题. 课程组就学员疑惑展开针对性对话,是一种线上私塾型编程教育. 
- 基于 Git/GitLab/Slack/邮件列表的真实课程环境,AKA(All know all,所有人知道所有人的代码提交,评论...),可以说有多少个开始折腾的学员,就有多少个子课程. 我认为,所有学员的轰动背后那个涌现的群体的'学员'更值得我们关注. 沉浸其中,你也许会发现社区之于人这一社会动物的意义所在. 
- 此外课程提炼了实施软件工程所需能力,流程,工具链,让我我们能够在尽可能短的时间内,整体感受软件开发流,以把握内心那股创造的悸动. 
- 蟒营织了一张安全网,学员只需花式折腾  :). 
    + 蟒营有一个迷之癖好,渴望真实的学员困惑. 并期待学员在有限的提示下,调用强悍的学习本能. 
    + 这困惑或许'傻'到---我的电脑是什么系统?
    + 蟒营或有心或无意挖了一众或深或浅的坑大胆尝试趟坑的人,回头再看,内心大多会有一句:
        * 我靠,原来我本来就很蟒. 


至于,蟒营名字来由,我觉得不仅因为 Python 本身和蟒有关,更取意生猛无畏. 


    懂懂懵懵 不知所当
    莽莽撞撞 遍体鳞伤
    智慧提问 寸进有望
    挂月百轮 过树千帆
    蟒蟒生威 好不风光

## Pythonic
> 蠎味儿 实例行为

- [#76](https://gitlab.com/101camp/1py/tasks/issues/76#note_168910180)
    + (请假推迟) @chengyuan101 
    + `激发点` 项目设计连载


# Recommedations  
~ 嗯哼各种场景中发现的嗯哼...

- 是也乎:
    + [ch05 知识图谱](http://io.101.camp/)


# Postscript 后记 
~ 蠎营周刊是什么以及为什么和能怎么...

大妈曰过: `参差多态 才是生机`

问题在 `参差` 的行为是无法形成团队的

    Coming together is a beginning; 
    Keeping together is progress; 
    Working together is success!

<--- [Henry Ford](https://www.brainyquote.com/quotes/quotes/h/henryford121997.html)

- 所以, 有了 大妈 随见随怼的持续嗯哼...
- 但是, 想象一年后, 回想几十周前自己作的那些 `图样图森破` 
    + 却没现成的资料来出示给后进来嗯哼?
    + 不科学, 值得记录的, 就应当有个形式固定下来
- 所以,有了这个 `蠎营周刊` (101Weekly)

:

    What is Pythonic?
    Why we make Pythonic?
    What are the possibilities of Pythonic?


That's why Dama keeps on debugging.
However, as time goes by, maybe you would not remember these days clearly and spread your experience difficultly.
What a pity!
The valuable should have a fixed form to be recorded.
That's why we make the Weekly for 101.camp.

