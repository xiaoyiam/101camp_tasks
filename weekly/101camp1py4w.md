# 101camp1py 课程周刊_v4
~ 预定 190513 2142 发布

-----------------------------------------

嗯哼

    自古行程半九十
    坚持艰难放弃易
    晴雨乱初夏

> DAMADAMA

-----------------------------------------

- 主编: [101Camp](https://gitlab.com/101camp)
- 责编: [DAMADAMA](https://gitlab.com/101camp/1py/tasks/wikis/dama/HbDamaUsage)


# Progress 
~ 课程活跃情况

是也乎,(￣▽￣)

- 虽然直接时, 学员们发言不积极
- 但是,文字提问时, 各种问题都能流畅嗯哼出来
- 这可能就是现场型口语自闭了...
- 当然, 不一定是问题, 但是, 一定是种阻力
- 毕竟文字输出过程中, 经过太多 模因(meme ~ 社会灌输给我们的各种预制思维)处置
    + 很可能和自己真正想问的偏差更多
- 所以, 一边说一边想, 才能逼自己真正心声吼出
- 另外, 其它所有有关编程的教程/图书可能都是错的...
    + 现在蟒营课程过程半
    + 大家应该有感觉了...
    + 仅仅关注编程语言/技术/框架/工具/...
    + 可能永远学不会编程
    + 就好象一名恋爱专家是通过研究言情小说以及级别考试而训练出来
    + 那是`注孤生`哪
- 以及, 完美主义一直是害死人的东西:
    + 无论任务/知识点/代码/...永远不要追求完美
    + 因为完美是不可能有确切指标来检验的
    + 只有可用 MVP 模型才是真实不虚的...
- 再再再再再再再次**提醒**:
    + 课程并不是每周任务都必须完成的
    + 和大家一起冲击当周任务才是节奏基础
    + 以及, 当周任务其实包含以往所有周任务核心要求了
    + 只要完成当周任务, 就等于以往所有周任务都达成了


![](chart/190513xkcd.png)


## 2 weekly activity:
> total commit: **68**


### top5 git commiter
> gen. 190513 221703.388 (by st v.190415.2042)


```
Bleu      : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 23.00
Hu        : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 14.00
hstaoqian : ▇▇▇▇▇▇▇▇▇▇ 10.00
nana      : ▇▇▇▇▇▇ 6.00 
maydayblue: ▇▇▇▇ 4.00 
```


### all Commit-Comments times
> gen. 2019-05-13 221703.457 (by st v.190415.2042)

```
huyuning    : ▇▇▇▇▇▇▇▇▇▇▇▇ 12.00
chengyuan101: ▇▇▇▇▇▇▇▇▇▇▇ 11.00
ZoomQuiet   : ▇▇▇▇▇▇▇▇▇▇ 10.00
near8023    : ▇▇▇▇ 4.00 
hstaoqian101: ▇ 1.00 
```


### all actions Issues
> gen. 2019-05-13 221703.066 (by st v.190415.2042)

```
ZoomQuiet   : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 415.00
huyuning    : ▇▇▇▇▇▇ 74.00
chengyuan101: ▇▇▇▇▇ 64.00
zheng2019   : ▇▇▇▇ 52.00
ljxzsx4     : ▇▇▇▇ 51.00
```



### words export for Issues
> gen. 2019-05-13 221703.226 (by st v.190415.2042)

```
ZoomQuiet   : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 97454.00
tanchangde  : ▇▇▇▇▇▇▇▇▇▇▇▇ 32654.00
charliewu101: ▇▇▇▇▇▇▇▇ 21814.00
chengyuan101: ▇▇▇▇▇▇▇ 20862.00
zheng2019   : ▇▇▇▇▇▇▇ 20143.00
```


# Stories 
~ 收集各自无法雷同的蠎营真人故事...

## BC
> 血案 ~ BloodCase 自然发生/撞开/生成/... 的工程案件

- [#57](https://gitlab.com/101camp/1py/tasks/issues/57#note_165090074)
- [#68](https://gitlab.com/101camp/1py/tasks/issues/68#note_166834461)
    + @huyuning 
    + ch04 任务描述小白化
    + 结果已经高速习惯了工程化描述感觉不到不可理解了...
- [#92](https://gitlab.com/101camp/1py/tasks/issues/92#note_168910945)
    + @nocynic 
    + csv 思维误区


## SM
> 隐藏任务激活

- [#65](https://gitlab.com/101camp/1py/tasks/issues/65)
    + @leon_xu 
    + web 界面 gitlab 功能巡演...
- [#74](https://gitlab.com/101camp/1py/tasks/issues/74#note_166834747)
    + @hstaoqian101 
    + 如何找到高质量的包


## Scenes
> 经典场景...

### WoW
> 不能不赞叹的...

- [#70](https://gitlab.com/101camp/1py/tasks/issues/70#note_166834515)
    + @charliewu101 
    + (推迟分享)科学上网
- [23595b11](https://gitlab.com/101camp/1py/tasks/commit/23595b1138ca40d41f02f5823c1b16319e6a456b#note_166835158)
    + @ljxzsx4 
    + 文章放入kindle, 并不完美, 但却是一个可行方向
- [#87](https://gitlab.com/101camp/1py/tasks/issues/87#note_168910658)
    + @chengyuan101 
    + 我的 MVP <-- 竟然是早已形成了习惯
- [#76](https://gitlab.com/101camp/1py/tasks/issues/76#note_168910180)
    + @chengyuan101 
    + (推迟分享)`激发点` 项目设计连载


### SaN
> See as Notsee ~ 视而不见


NIL

## Pythonic
> 蠎味儿 实例行为


...正如我周会Q&A所说:

    当我知道我要实现什么功能,然后我去Google搜索,可以找到相关语句支撑. 
    但是对于语句本身是怎么运作的,我了解得很少. 
    所以当满足复杂功能的一整段语句出现时,我基本上看不明白. 
    (其中涉及到结构和单个语句的问题)


其实这种感觉在进入任何一个陌生领域知识时, 都是相同的:

- 好比开始学习英文时
    + 刚刚知道字母以及如何查字典,
    + 而掌握的单词没超过42个时,
- 看英文名著当然是根本看不下去, 因为几乎每一句什么意思, 都要查字典...
- 那么, 我们如何跨过这一阶段的?
    + 多查字典
    + 多看文章
    + 多写文章
    + 多背名句
    + ...
    + 是的, 非常非常笨以及老土的方法
- 编程也一样, 而 Python 在这方面其实对中国人有非常友好的地方:
    + Python 是已知语言中最接近自然英文的开发语言之一
    + 如果一篇脚本的变量名设计好
    + 几乎可以当成比较怪的作文来看
    + 毕竟, Python 原型 ABC 语言的设计目标就是儿童教育...
- 所以, 阅读代码本身就是编程的一个重要环节
    + 在阅读他人代码的过程中
    + 吸收软件设计/工程管理/行业惯用/命名学/... 的知识
    + 当然, 优先应该阅读的是优秀 Python 作品的代码
        * 什么叫优秀?
        * github 中 start 超过 1000 的一般可以算是...
    + 以及, 当前同学间窜门, 又不同查阅现成软件代码
        * 同学们完成的代码都是为实现相同功能
        * 但是,用了自己掌握知识点的组合
        * 可以说就是回字的6种写法
        * 这对你来说, 可能就不是看细节, 而是多看整体
        * 大家如何组织代码的? 如何切分函式的? 如何设计变量名的?...
- 以及: `...满足复杂功能的一整段...`
    + 对于复杂的直觉你是对的
    + 也正如直播交流中俺反馈的
    + 代码第一读者是人, 而不是机器
    + 所以, 简洁直白的代码才是好代码
    + 但是, 直白的定义, 是随着学习在变化的
    + 好比, 英文学习中
        * 开始,只有 this is, that is, 这种4个单词以内固定句式的对自己算真白
        * 但是, 随着经验/语感的积累, 感觉直白的句子,其实在逐渐变得复杂起来..
    + 所以, 当前这种状态是自然的,必然的,不可以忽略的...
- 如何快速冲破这种拘束状态?
    + 坚持...每天尝试写/运行/理解自己的代码
    + 坚持...每天尝试写/运行/理解同学的代码
    + 坚持...每天尝试写/运行/理解找来的代码
    + ...直到习惯 Python 的形式
- 当然, 课程可以提供的切实辅助就是:
    + 在这个坚持过程中
    + 自己无法理解的任何一个知识点/形式/模块/概念/....
    + 都可以及时来问
        * 以 Issue 的正式形式
        * 以 Issue 的正式形式
        * 以 Issue 的正式形式
        * ... 是的正如 提问的智慧 中给出的经验
            - 前提是只有你认真
            - 大家才可能认真
            - 以上无它

# Recommedations  
~ 嗯哼各种场景中发现的嗯哼...

- 是也乎:
    + [ch04 知识图谱](http://io.101.camp/)
    + [Issue 367 ~蠎周刊 ~汇集全球蠎事儿 ;-)](http://weekly.pychina.org/issue/issue-367.html)


# Postscript 后记 
~ 蠎营周刊是什么以及为什么和能怎么...

大妈曰过: `参差多态 才是生机`

问题在 `参差` 的行为是无法形成团队的

    Coming together is a beginning; 
    Keeping together is progress; 
    Working together is success!

<--- [Henry Ford](https://www.brainyquote.com/quotes/quotes/h/henryford121997.html)

- 所以, 有了 大妈 随见随怼的持续嗯哼...
- 但是, 想象一年后, 回想几十周前自己作的那些 `图样图森破` 
    + 却没现成的资料来出示给后进来嗯哼?
    + 不科学, 值得记录的, 就应当有个形式固定下来
- 所以,有了这个 `蠎营周刊` (101Weekly)

:

    What is Pythonic?
    Why we make Pythonic?
    What are the possibilities of Pythonic?


That's why Dama keeps on debugging.
However, as time goes by, maybe you would not remember these days clearly and spread your experience difficultly.
What a pity!
The valuable should have a fixed form to be recorded.
That's why we make the Weekly for 101.camp.

