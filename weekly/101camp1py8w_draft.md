# 101camp1py 课程周刊_v8
~ 预定 190610 2142 发布

-----------------------------------------

定场诗

    ...

> ???

-----------------------------------------

- 主编: [101Camp](https://gitlab.com/101camp)
- 责编: [DAMADAMA](https://gitlab.com/101camp/1py/tasks/wikis/dama/HbDamaUsage)


# Progress 
~ 课程活跃情况


# Stories 
~ 收集各自无法雷同的蠎营真人故事...

## BC
> 血案 ~ BloodCase 自然发生/撞开/生成/... 的工程案件

NIL

## SM
> 隐藏任务激活

NIL


## Scenes
> 经典场景...

### WoW
> 不能不赞叹的...

NIL

### SaN
> See as Notsee ~ 视而不见


NIL

## Pythonic
> 蠎味儿 实例行为

NIL


# Recommedations  
~ 嗯哼各种场景中发现的嗯哼...

- 是也乎:
    + [ch08 知识图谱](http://io.101.camp/)


# Postscript 后记 
~ 蠎营周刊是什么以及为什么和能怎么...

大妈曰过: `参差多态 才是生机`

问题在 `参差` 的行为是无法形成团队的

    Coming together is a beginning; 
    Keeping together is progress; 
    Working together is success!

<--- [Henry Ford](https://www.brainyquote.com/quotes/quotes/h/henryford121997.html)

- 所以, 有了 大妈 随见随怼的持续嗯哼...
- 但是, 想象一年后, 回想几十周前自己作的那些 `图样图森破` 
    + 却没现成的资料来出示给后进来嗯哼?
    + 不科学, 值得记录的, 就应当有个形式固定下来
- 所以,有了这个 `蠎营周刊` (101Weekly)

:

    What is Pythonic?
    Why we make Pythonic?
    What are the possibilities of Pythonic?


That's why Dama keeps on debugging.
However, as time goes by, maybe you would not remember these days clearly and spread your experience difficultly.
What a pity!
The valuable should have a fixed form to be recorded.
That's why we make the Weekly for 101.camp.

