# 101camp1py 课程周刊_v0
~ 预定 190415 2142 发布

-----------------------------------------

101

    要现在开始编程
    要现在开始提问
    云开谷雨时

> DAMADAMA

-----------------------------------------

- 主编: [101Camp](https://gitlab.com/101camp)
- 责编: [DAMADAMA](https://gitlab.com/101camp/1py/tasks/wikis/dama/HbDamaUsage)


# Progress 
~ 课程活跃情况

嗯哼, 101camp1py 按照开课了:

- 这次比原型班, 多了两周报名时间, 而且多了10天预习时间
- 这导致, 在没正式开始课程内容时:
    + 就已经产生了大量探索行为
    + 也导致最后一天报名进来的学员感受到异常的压力
    + 以至, 首次出现退课现象
- 不过, 现在想来, 这也算幸事儿:
    + 说明, 大家对于自己的状态和期待非常清晰
    + 并且, 也能独立决断马上退出
    + 这样, 总比一直忍耐, 课外反复 diss 课程多没人性...etc. 要来的简单友好
- 通过开课直播, 了解到:
    + 医疗领域学员不少
    + 以转职为目标的学员更多
    + 有明确目标开发对象的不多...
    + 这在一开始是很自然的:
        * 因为, 并不知道代码真能作到什么程度
        * 因为, 还不能确认自己写的代码能作到什么程度
- 只是, 想分享一点常识:
    + 所谓, 专业/职业, 其实就是能主动将一件事儿按时作完
    + 无论这事儿是成功还是失败
    + 总是作完, 不拖不推诿, 能担当
    + 这便是专业的开始
    + 否则, 永远有越来越多悬而末决的事儿拖住自己
    + 那么, 什么都学不到, 作不出....
- 所以, 其实蟒营另外一个 Slogan 就是:
    + **Fake it untill make it ;-)**


![](chart/190415xkcd.png)

## 2 weekly activity:
> total commit: **120**


### top5 git commiter
> gen. 190415 203312.290 (by st v.190415.2042)

```
Zoom.Quiet  : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 45.00
Changde     : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 19.00
linfeng     : ▇▇▇▇▇▇▇▇▇ 12.00
zhouyinyun  : ▇▇▇▇▇▇▇ 9.00 
charliewu101: ▇▇▇▇ 5.00 
```


### all Commit-Comments times
> gen. 2019-04-15 203312.379 (by st v.190415.2042)

```
ZoomQuiet   : ▇▇▇▇▇▇▇▇▇▇ 10.00
tanchangde  : ▇▇▇▇▇ 5.00 
maydayblue  : ▇▇▇ 3.00 
charliewu101: ▇ 1.00 
hstaoqian101: ▇ 1.00 
```


### all actions Issues
> gen. 2019-04-15 203311.987 (by st v.190415.2042)

```
ZoomQuiet   : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 81.00
chengyuan101: ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 34.00
zheng2019   : ▇▇▇▇▇▇▇▇ 18.00
huyuning    : ▇▇▇▇▇▇▇ 17.00
zhouyinyun  : ▇▇▇▇▇ 13.00

```

### words export for Issues
> gen. 2019-04-15 203312.141 (by st v.190415.2042)

```
ZoomQuiet   : ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 25791.00
chengyuan101: ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 13472.00
zhouyinyun  : ▇▇▇▇▇▇▇▇▇▇▇▇▇ 9643.00
tanchangde  : ▇▇▇▇▇▇▇▇▇▇ 7637.00
zheng2019   : ▇▇▇▇▇▇▇▇▇▇ 7172.00
```



# Stories 
~ 收集各自无法雷同的蠎营真人故事...

## BC
> 血案 ~ BloodCase 自然发生/撞开/生成/... 的工程案件

[#29](https://gitlab.com/101camp/1py/tasks/issues/29) tasks.wiki 的大量删除 

## SM
> 隐藏任务激活


- [#4](https://gitlab.com/101camp/1py/tasks/issues/4#note_158186932)
    + @zheng2019 Slack vs 微信 ?
- [#3](https://gitlab.com/101camp/1py/tasks/issues/3#note_158175465)
    + @L-luffy `[SM]最简日常 git 操作指北`
- [#5](https://gitlab.com/101camp/1py/tasks/issues/5#note_158415372)
    + @nocynic `蟒营课程用最简 Markdown 实用指北`
- [#9](https://gitlab.com/101camp/1py/tasks/issues/9#note_159181830)
    + @chengyuan101 Pyenv?


## Scenes
> 经典场景...

### WoW
> 不能不赞叹的...


- [#8](https://gitlab.com/101camp/1py/tasks/issues/8#note_158721253)
    + @near8023 首杀+slack提醒缺漏发现

### SaN
> See as Notsee ~ 视而不见

- [#19](https://gitlab.com/101camp/1py/tasks/issues/19) 邮件信头在哪里?
    + 主动解决了问题
    + 但是, 也点出多数学员从来没真正注目过自己收到邮件的所有信息.

## Pythonic
> 蠎味儿 实例行为

[git无法添加空目录?](https://gitlab.com/101camp/1py/tasks/issues/3)

- 发现问题 -> 解决问题 -> end
- 这是经典的工程师行为
- 只是, 在蟒营, 期望多一点儿:
    + 发现问题 
    + -> 解决问题 
    + -> 分享知识 -> 在更多场景中实践 -> 触发更多有趣的问题, 一起来嗯哼
    + 是的, 一但开始探索, 永远停不下来哈.

# Recommedations  
~ 嗯哼各种场景中发现的嗯哼...

- 是也乎:
    + [Issue 363 \|蠎周刊 \|汇集全球蠎事儿 ;-)](http://weekly.pychina.org/issue/issue-363.html)
    + [ch00 知识图谱](http://io.101.camp/)


# Postscript 后记 
~ 蠎营周刊是什么以及为什么和能怎么...

大妈曰过: `参差多态 才是生机`

问题在 `参差` 的行为是无法形成团队的

    Coming together is a beginning; 
    Keeping together is progress; 
    Working together is success!

<--- [Henry Ford](https://www.brainyquote.com/quotes/quotes/h/henryford121997.html)

- 所以, 有了 大妈 随见随怼的持续嗯哼...
- 但是, 想象一年后, 回想几十周前自己作的那些 `图样图森破` 
    + 却没现成的资料来出示给后进来嗯哼?
    + 不科学, 值得记录的, 就应当有个形式固定下来
- 所以,有了这个 `蠎营周刊` (101Weekly)

:

    What is Pythonic?
    Why we make Pythonic?
    What are the possibilities of Pythonic?


That's why Dama keeps on debugging.
However, as time goes by, maybe you would not remember these days clearly and spread your experience difficultly.
What a pity!
The valuable should have a fixed form to be recorded.
That's why we make the Weekly for 101.camp.

